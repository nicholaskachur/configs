These are the dot files, of the starship Nicholas.

There's nothing terribly fancy in here for now, just slightly modified versions of Cygwin's default bashrc, my tmux.conf, and a decent vimrc. Feel free to borrow anything useful, and get in touch if it breaks!

NOTE: All the files in this repository are dotfiles, so you'll have to use 'ls -a' to view them all.
